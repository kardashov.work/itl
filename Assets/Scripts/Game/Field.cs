﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class Field
{
    private Vector2 size;
    private List<Item> items;
    private Transform parentTransform;
    private ResourcesConfig resourcesConfig;

    public Field(Vector2 size)
    {
        this.size = size;
        resourcesConfig = GameManager.Instance.ResourcesConfig;
        parentTransform = new GameObject("Field").transform;
        GenerateItems();
    }

    private void GenerateItems()
    {
        items = new List<Item>();

        for (int x = 0; x < size.x; x++)
        {
            for (int y = 0; y < size.y; y++)
            {
                var type = resourcesConfig.GetRandomItemType();
                var color = resourcesConfig.GetRandomColor();
                Item item = new Item($"Item (x:{x}, y:{y})", parentTransform, new Vector2(x, y));
                items.Add(item);
            }
        }
    }

    public Item GetItem(Vector2 position)
    {
        return items.Where(it => it.GetPosition().Equals(position)).FirstOrDefault();
    }

    public Item GetItem(Transform transform)
    {
        if (transform == null)
            return null;

        return GetItem(transform.position);
    }

    public List<Item> GetItemNeighbors(Item item)
    {
        var neighbors = items.Where(it => GetFieldNeighborsPositions(item).Contains(it.GetFieldPosition())).ToList();
        neighbors.RemoveAll(it => it.IsEmpty);
        return neighbors;
    }

    private List<Vector2> GetFieldNeighborsPositions(Item item)
    {
        var fp = item.GetFieldPosition();
        List<Vector2> result = new List<Vector2>()
        {
            new Vector2(fp.x + 1, fp.y),
            new Vector2(fp.x - 1, fp.y),
            new Vector2(fp.x, fp.y + 1),
            new Vector2(fp.x, fp.y - 1),
        };

        return result;
    }

    public void ChangeColorToRandom(List<Item> items)
    {
        foreach (var item in items)
        {
            item.Color = resourcesConfig.GetRandomColor();
        }
    }

    public void ChangeNeighborsColor(List<Item> items, Color color)
    {
        foreach (var item in items)
        {
            ChangeColor(GetItemNeighbors(item), color);
        }
    }

    public void ChangeColor(Item item, Color color)
    {
        item.Color = color;
    }

    public void ChangeColor(List<Item> items, Color color)
    {
        foreach (var item in items)
        {
            ChangeColor(item, color);
        }
    }

    public void RespawnItem(Item item)
    {
        item.SetEmpty();
        item.Type = resourcesConfig.GetRandomItemType();
        item.Color = resourcesConfig.GetRandomColor();
    }

    public void RespawnItems(List<Item> items)
    {
        foreach (var item in items)
        {
            RespawnItem(item);
        }
    }

    public void SpawnRandomItem() //TODO: Mb use while
    {
        var emptyItems = items.Where(it => it.IsEmpty).ToArray();
        var emptyCells = emptyItems.Length;
        var n =  -emptyCells + (resourcesConfig.GameConfig.MaxItemsOnField * 2);
        if (n > 0)
        {
            var item = emptyItems[Random.Range(0, emptyCells)];
            item.Type = resourcesConfig.GetRandomItemType();
            item.Color = resourcesConfig.GetRandomColor();
        }      
    }

    public void SpawnRandomItems(int count)
    {
        for (int i = 0; i < count; i++)
        {
            SpawnRandomItem();
        }
    }

    public void ResetItem(Item item)
    {
        item.SetEmpty();
    }

    public void ResetItems(List<Item> items)
    {
        foreach (var item in items)
        {
            item.SetEmpty();
        }
    }

    public List<Item> GetItems(BaseItem item)
    {
        return items.Where(it => it.Equals(item)).ToList();
    }

    public BaseItem GetRandomItem()
    {
        var arr = items.Where(it => !it.IsEmpty).ToArray();
        return arr[Random.Range(0, arr.Length)];
    }

    public void Reset()
    {
        foreach (var item in items)
        {
            item.SetEmpty();
        }
    }
}
