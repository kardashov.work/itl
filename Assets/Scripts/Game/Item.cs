﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item: BaseItem
{
    private GameObject gameObject;
    private Transform transform;
    private SpriteRenderer spriteRenderer;
    private Vector2 position;
    private float shapeSize;

    public string Name
    {
        get { return gameObject.name; }
    }

    public override Color Color
    {
        get
        {
            return color;
        }
        set
        {
            color = value;
            if(spriteRenderer)
                spriteRenderer.color = color;
        }
    }

    public override ItemType Type
    {
        get
        {
            return type;
        }
        set
        {
            type = value;
            if (spriteRenderer)
                spriteRenderer.sprite = resourcesConfig.GetItemSprite(type);
        }
    }

    public Item(string name, Transform parent, Vector2 position) : base()
    {
        InitGameObject(name, parent);

        this.position = position;
        SetPosition(this.position);
        
        Type = ItemType.NONE;
        Color = resourcesConfig.EmptyColor;
    }

    public Item(string name, Transform parent, Vector2 position, ItemType itemType, Color color): base(itemType, color)
    {
        InitGameObject(name, parent);

        Type = itemType;
        Color = color;
        this.position = position;        
        SetPosition(this.position);
    }    

    private void InitGameObject(string name, Transform parent)
    {
        gameObject = GameObject.Instantiate<GameObject>(resourcesConfig.ItemPrefab);
        gameObject.name = name;
        transform = gameObject.transform;
        transform.parent = parent;
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        shapeSize = spriteRenderer.bounds.size.x;
    }

    public void SetEmpty()
    {
        Type = ItemType.NONE;
        Color = resourcesConfig.EmptyColor;
    }

    public bool IsEmpty
    {
        get { return Type == ItemType.NONE; }
    }

    public void SetPosition(Vector2 position)
    {
        position *= shapeSize + (shapeSize / 2);
        transform.position = position;
    }

    public Vector2 GetFieldPosition()
    {
        return position;
    }
    
    public Vector2 GetPosition()
    {
        return transform.position;
    }   
}
