﻿using System;
using UnityEngine;

public class User
{
    public Action<int> OnScoreUpdated;
    public Action<string> OnNameChanged;

    private string name;
    public string Name
    {
        get
        {
            return name;
        } 
        set
        {
            if (name != null && !name.Equals(value))
            {
                SaveSystem.SetKey<string>(SaveSystem.USER_NAME_KEY, value);
                OnNameChanged?.Invoke(value);
            }
            name = value;      
        }
    }
    
    private int score;
    public int Score
    {
        get
        {
            return score;
        }
        set
        {
            score = value;
            score = Mathf.Clamp(score, 0, int.MaxValue);
            OnScoreUpdated?.Invoke(score);

            if(score > BestScore)
            {
                BestScore = score;
            }
        }
    }

    private int bestScore;
    public int BestScore
    {
        get
        {
            return bestScore;
        }
        set
        {
            if(bestScore != value)
            {
                SaveSystem.SetKey(SaveSystem.BEST_SCORE_KEY, value);
            }
            bestScore = value;
        }
    }

    public User()
    {
        Name = SaveSystem.GetKey<string>(SaveSystem.USER_NAME_KEY);
        BestScore = SaveSystem.GetKey<int>(SaveSystem.BEST_SCORE_KEY);
    }
}