﻿using UnityEngine;

public class BaseItem
{
    protected ItemType type;
    public virtual ItemType Type
    {
        get { return type; }
        set { type = value; }
    }

    protected Color color;
    public virtual Color Color
    {
        get { return color; }
        set { color = value; }
    }

    protected ResourcesConfig resourcesConfig
    {
        get { return GameManager.Instance.ResourcesConfig; }
    }

    public BaseItem()
    {
        Type = ItemType.NONE;
        Color = resourcesConfig.EmptyColor;
    }

    public BaseItem(ItemType type, Color color)
    {
        Type = type;
        Color = color;
    }

    public override bool Equals(object obj)
    {
        if (obj is BaseItem item)
        {
            return Color == item.Color && Type == item.Type;
        }
        else
        {
            return false;
        }
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override string ToString()
    {
        return base.ToString();
    }
}
