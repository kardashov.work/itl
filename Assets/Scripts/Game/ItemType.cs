﻿
public enum ItemType
{
    NONE = -1,
    Circle = 0,
    Square = 1,
    Triangle = 2
}