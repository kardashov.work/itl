﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
public class UIManager : Singleton<UIManager>
{
    public static UIResources Resources
    {
        get { return GameManager.Instance.UIResources; }
    }

    public UIPopup ActivePopup { get; private set; }

    private List<MonoBehaviour> cachedObjects = new List<MonoBehaviour>();

    private Transform canvasTransform;
    private Transform CanvasTransform
    {
        get
        {
            if(canvasTransform == null)
            {
                canvasTransform = FindObjectOfType<Canvas>().transform;
            }

            return canvasTransform;
        }
    }

    private void Start()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        cachedObjects.RemoveAll(obj => obj == null);
    }

    public void ShowPopup(UIPopup popup, string caption, string message, UnityAction onOk)
    {
        CloseOpenedPopup();
        ActivePopup = GetCachedObject(popup);
        ActivePopup.Show(caption, message, onOk);
    }

    public void ShowPopup(UIPopup popup, string caption, string message)
    {
        CloseOpenedPopup();
        ActivePopup = GetCachedObject(popup);
        ActivePopup.Show(caption, message);
    }

    public void ShowPopup(UIPopup popup)
    {
        CloseOpenedPopup();
        ActivePopup = GetCachedObject(popup);
        ActivePopup.Show();
    }

    public void CloseOpenedPopup()
    {
        if (ActivePopup == null) return;

        ActivePopup.Close();
        ActivePopup = null;        
    }

    private T CreateInstance<T>(T obj) where T: MonoBehaviour
    {
        var instance = Instantiate(obj, CanvasTransform);
        instance.name = obj.name;
        return instance;
    }

    private T GetCachedObject<T>(T obj) where T : MonoBehaviour
    {
        var instance = cachedObjects.Where(p => p.name.Equals(obj.name)).FirstOrDefault();
        if (instance == null)
        {
            instance = CreateInstance<T>(obj);
            cachedObjects.Add(instance);
        }

        return (T)instance;
    }
}
