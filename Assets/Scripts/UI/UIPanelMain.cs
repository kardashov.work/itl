﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIPanelMain : MonoBehaviour
{
    [Header("Labels")]
    [SerializeField] Text userName;
    [SerializeField] Text bestScore;

    [Space]
    [Header("Buttons")]
    [SerializeField] Button playBtn;
    [SerializeField] Button editNameBtn;
    [SerializeField] Button quitBtn;

    private GameManager gameManager;

    private void Start()
    {
        gameManager = GameManager.Instance;        
        gameManager.User.OnNameChanged += ChangeName;
        SetUIData();
        AddButtonsActions();
    }

    private void OnDestroy()
    {
        gameManager.User.OnNameChanged -= ChangeName;
    }

    private void ChangeName(string name)
    {
        userName.text = name;
    }

    private void SetUIData()
    {
        var user = gameManager.User;
        if (!string.IsNullOrEmpty(user.Name))
        {
            userName.text = $"User: {user.Name}";
            bestScore.text = $"Best score: {user.BestScore}";
        }
    }

    private void AddButtonsActions()
    {
        playBtn.onClick.AddListener(() => //TODO: Create function
        {
            SceneManager.LoadScene("Game");
        });

        editNameBtn.onClick.AddListener(() => //TODO: Create function
        {
            UIManager.Instance.ShowPopup(UIManager.Resources.ChangeUserName);
        });

        quitBtn.onClick.AddListener(() =>
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        });
    }
}
