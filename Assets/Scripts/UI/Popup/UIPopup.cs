﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIPopup : MonoBehaviour
{
    [SerializeField]
    Text captionText;

    [SerializeField]
    Text descriptionText;

    [SerializeField]
    protected Button closeBtn;

    [SerializeField]
    protected Button applyBtn;

    private UnityAction OnOkAction;
    private UnityAction OnCanselAction;

    public void Show(string caption, string message, UnityAction onOkAction)
    {
        OnOkAction = onOkAction;
        Show(caption, message);
    }

    public void Show(string caption, string message)
    {
        captionText.text = caption;
        descriptionText.text = message;
        Show();
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }

    protected void Start()
    {
        closeBtn.onClick.AddListener(OnClose);
        applyBtn.onClick.AddListener(OnApply);
    }

    private void OnClose()
    {
        OnCanselAction?.Invoke();
        UIManager.Instance.CloseOpenedPopup();
    }

    private void OnApply()
    {
        OnOkAction?.Invoke();
        UIManager.Instance.CloseOpenedPopup();
    }
}
