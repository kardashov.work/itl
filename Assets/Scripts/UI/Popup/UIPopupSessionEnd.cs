﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIPopupSessionEnd : UIPopup
{
    private void Start()
    {
        base.Start();
        applyBtn.onClick.AddListener(() => { GameManager.Instance.Replay(); });
        closeBtn.onClick.AddListener(() => { SceneManager.LoadScene(0); });
    }
}
