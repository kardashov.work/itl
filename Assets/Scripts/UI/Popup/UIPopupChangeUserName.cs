﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupChangeUserName : UIPopup
{
    [SerializeField]
    InputField nameInputField;    

    private void Start()
    {
        base.Start();
        applyBtn.onClick.AddListener(ApplyNewName);        
    }

    private void ApplyNewName()
    {
        var newName = nameInputField.text;
        nameInputField.text = string.Empty;
        if (newName.Length <= 0)
        {
            Debug.LogError("The username shall be at least 1-20 characters' long.");
            newName = "IncorectName";
        }

        GameManager.Instance.User.Name = newName;
    }
}
