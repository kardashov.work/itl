﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UITopPanel : MonoBehaviour
{
    [SerializeField] Text userNameText;
    [SerializeField] Text timerText;
    [SerializeField] Text scoreText;
    [SerializeField] Image targetItem;
    [SerializeField] Button backButton;

    private Timer timer;

    GameManager gameManager;

    private void Awake()
    {
        gameManager = GameManager.Instance;
        gameManager.User.OnScoreUpdated += UpdateScore;
        gameManager.OnTargetGenerated += UpdateTarget;
        backButton.onClick.AddListener(Quit);
    }

    private void OnDestroy()
    {
        gameManager.User.OnScoreUpdated -= UpdateScore;
        gameManager.OnTargetGenerated -= UpdateTarget;
        backButton.onClick.RemoveListener(Quit);
    }

    private void Start()
    {
        gameManager.OnReplay += Init;
        Init();
    }

    private void Quit()
    {
        UIManager.Instance.ShowPopup(UIManager.Resources.Popup, "Quit", "Are you sure you want to quit", ()=> 
        {
            SceneManager.LoadScene(0);
        });
    }

    private void Init()
    {
        scoreText.text = "Score: 0";
        userNameText.text = gameManager.User.Name;
        timer = gameManager.sessionTimer;
    }


    private void Update()
    {
        if (timer != null)
        {
            var tr = (int)timer.GetTimeRemaining();
            timerText.text = $"Time left: {tr}s";
        }
    }

    private void UpdateScore(int score)
    {
        scoreText.text = $"Score: {score}";
    }

    private void UpdateTarget(BaseItem item)
    {
        targetItem.sprite = gameManager.ResourcesConfig.GetItemSprite(item.Type);
        targetItem.color = item.Color;
    }

    
}
