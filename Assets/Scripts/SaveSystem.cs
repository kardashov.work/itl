﻿using System;
using UnityEngine;

public static class SaveSystem
{
    public const string BEST_SCORE_KEY = "BEST_SCORE";
    public const string USER_NAME_KEY = "USER_NAME";

    public static T GetKey<T>(string key)
    {
        var value = PlayerPrefs.GetString(key, string.Empty);

        if (string.IsNullOrEmpty(value))
            return default(T);

        return (T)Convert.ChangeType(value, typeof(T));
    }

    public static void SetKey<T>(string key, T value)
    {
        var val = value.ToString();

        PlayerPrefs.SetString(key, val);
        ApplySave();
    }

    public static void DeleteAll()
    {
        PlayerPrefs.DeleteAll();
        ApplySave();
    }

    private static void ApplySave()
    {
        PlayerPrefs.Save();

        // TODO: Add cloud save
    }
}
