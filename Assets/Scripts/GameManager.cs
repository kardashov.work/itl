﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    public GameConfig GameConfig { get; private set; }
    public UIResources UIResources { get; private set; }
    public ResourcesConfig ResourcesConfig { get; private set; }

    private Field field;

    private BaseItem targetItem;

    private User user;
    public User User
    {
        get
        {
            if (user != null)
            {
                return user;
            }
            else
            {
                user = new User();
                return user;
            }
        }
    }

    public Action<BaseItem> OnTargetGenerated;    
    public Action OnReplay;

    public Timer sessionTimer;
    private Timer spawnTargetTimer;
    private Timer spawnIemsTimer;

    private void Awake()
    {
        Init();
    }

    private void Init()
    {
        ResourcesConfig = Resources.Load<ResourcesConfig>("Configs/RuntimeResources");
        GameConfig = ResourcesConfig.GameConfig;
        UIResources = ResourcesConfig.UIResources;
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if(scene.buildIndex == 1)
        {
            field = new Field(GameConfig.FieldSize);
            Play();
        }
    }

    private void RegisterTimers()
    {
        sessionTimer = Timer.Register(GameConfig.SessionTime, OnSessionEnd);
        spawnTargetTimer = Timer.Register(GameConfig.TargetLifeTime, GenerateRandomTarget, null, true);
        spawnIemsTimer = Timer.Register(GameConfig.SpawnItemsDelay, SpawnItems, null, true);
    }

    public void Replay()
    {
        field.Reset();
        Play();
        OnReplay?.Invoke();
    }

    public void Play()
    {
        User.Score = 0;
        RegisterTimers();
        SpawnItems();
        GenerateRandomTarget();
    }

    private bool IsUserWin
    {
        get { return User != null && User.Score >= GameConfig.ScoreToWin; }
    }

    private void Update()
    {
        if (UIManager.Instance.ActivePopup != null)
            return;

        var selectedItem = GetItemOnClick();
        if(selectedItem != null && !selectedItem.IsEmpty)
        {
            if (selectedItem.Equals(targetItem))
            {
                User.Score += GameConfig.CorrectTargetScore;

                var items = field.GetItems(selectedItem);
                field.ChangeNeighborsColor(items, selectedItem.Color);
                field.ResetItems(items);
            }
            else
            {
                User.Score += GameConfig.IncorrectTargetScore;
                
                field.ChangeColorToRandom(field.GetItems(selectedItem));
                field.ResetItem(selectedItem);
            }

            targetItem = null;
            spawnTargetTimer.Restart();
            GenerateRandomTarget();            
        }
    }
    
    private void SpawnItems()
    {
        field.SpawnRandomItems(GameConfig.InstantiateObjectsOnStep);     
    }

    private Item GetItemOnClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit.collider != null)
            {
                return field.GetItem(hit.transform);
            }
        }

        return null;
    }

    private void GenerateRandomTarget()
    {
        targetItem = field.GetRandomItem();
        OnTargetGenerated?.Invoke(targetItem);
    }
    
    private void OnSessionEnd()
    {
        if (IsUserWin)
        {
            UIManager.Instance.ShowPopup(UIResources.SessionEnd, "Congratulation", $"You win!\nScore: {User.Score}");
        }
        else
        {
            UIManager.Instance.ShowPopup(UIResources.SessionEnd, "Failed...", $"Score: {User.Score}");
        }        

        Timer.CancelAllRegisteredTimers();
    }

    
}

