﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "ILT/Game Config")]
public class GameConfig : ScriptableObject
{
    [Header("Config")]
    [SerializeField] Vector2 fieldSize;
    public Vector2 FieldSize { get { return fieldSize; } }

    [SerializeField] int maxItemsOnField;
    public int MaxItemsOnField { get { return maxItemsOnField; } }

    [SerializeField] int[] maxItemsOnFieldByType;
    public int[] MaxItemsOnFieldByType { get { return maxItemsOnFieldByType; } }

    [SerializeField] int colorsCount;
    public int ColorsCount { get { return colorsCount; } }

    [SerializeField] int instantiateObjectsOnStep;
    public int InstantiateObjectsOnStep { get { return instantiateObjectsOnStep; } }

    [SerializeField] float sessionTime;
    public float SessionTime { get { return sessionTime; } }

    [SerializeField] int scoreToWin;
    public int ScoreToWin { get { return scoreToWin; } }

    [SerializeField] int incorrectTargetScore;
    public int IncorrectTargetScore { get { return incorrectTargetScore; } }

    [SerializeField] int correctTargetScore;
    public int CorrectTargetScore { get { return correctTargetScore; } }

    [SerializeField] int decreaseScoreByTime;
    public int DecreaseScoreByTime { get { return decreaseScoreByTime; } }

    [SerializeField] float targetLifeTime;
    public float TargetLifeTime { get { return targetLifeTime; } }

    [SerializeField] float spawnItemsDelay;
    public float SpawnItemsDelay { get { return spawnItemsDelay; } }

}
