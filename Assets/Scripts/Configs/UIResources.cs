﻿using UnityEngine;
using System.Collections;
using System;
using Random = UnityEngine.Random;

[CreateAssetMenu(menuName = "ILT/UI Resources")]
public class UIResources : ScriptableObject
{
    [Header("Popup")]
    [SerializeField]
    UIPopup popup;
    public UIPopup Popup { get { return popup; } }

    [SerializeField]
    UIPopup chamgeUserName;
    public UIPopup ChangeUserName { get { return chamgeUserName; } }

    [SerializeField]
    UIPopup sessionEnd;
    public UIPopup SessionEnd { get { return sessionEnd; } }
}
