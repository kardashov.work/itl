﻿using UnityEngine;
using System.Collections;
using System;
using Random = UnityEngine.Random;

[CreateAssetMenu(menuName = "ILT/Resources Config")]
public class ResourcesConfig : ScriptableObject
{
    [Space]
    [Header("Configs")]
    [SerializeField]
    GameConfig gameConfig;
    public GameConfig GameConfig
    {
        get { return gameConfig; }
    }

    [SerializeField]
    UIResources uiResources;
    public UIResources UIResources
    {
        get { return uiResources; }
    }

    [Space]
    [Header("Items")]
    [SerializeField]
    GameObject itemPrefab;
    public GameObject ItemPrefab
    {
        get { return itemPrefab; }
    }

    [SerializeField]
    Sprite[] shapes;

    [Space]
    [Header("Colors")]
    [SerializeField]
    Color emptyColor = new Color(0, 0, 0, .1f);
    public Color EmptyColor { get { return emptyColor; } }

    [SerializeField]
    Color[] colors;
    public Color[] Colors
    {
        get { return colors; }
    }

    public Color GetRandomColor()
    {
        return colors[Random.Range(0, colors.Length)];
    }

    public ItemType GetRandomItemType()
    {
        var t = Random.Range(0, ShapesCount);
        return (ItemType)t;
    }

    public int ShapesCount
    {
        get { return shapes.Length; }
    }

    public Sprite GetItemSprite(ItemType itemType)
    {
        var index = (int)itemType;
        if (index == -1)
            index = 1;

        return shapes[index];
    }

    [ContextMenu("Randomize colors")]
    void RandomizeColors()
    {
        if(gameConfig == null)
        {
            Debug.LogError("Game config not found!");
        }
        else
        {
            var count = gameConfig.ColorsCount;
            colors = new Color[count];
            for (int i = 0; i < count; i++)
            {
                colors[i] = Random.ColorHSV();
            }
        }        
    }

    public void OnValidate()
    {
        if(colors.Length > gameConfig.ColorsCount)
        {
            Debug.LogError($"Invalid colors count: {colors.Length}!\nMax colors count is: {gameConfig.ColorsCount}");
            Array.Resize(ref colors, gameConfig.ColorsCount);
        }
    }
}
